﻿---
title: 'We are Legion: Rome'
layout: project_post
published: true
post-image: "https://cdn.cloudflare.steamstatic.com/steam/apps/1120790/header.jpg"
description: Adventure-story driven first person medieval game about roman legion.
---

**Technologies:**<br>
- Unity
  <br>
- C#
  <br>
- Behaviour Designer
  <br>
- UniRX

---

So due to the project being suspended (and lack of proper management) there are no pictures or videos published about this project that I can show.
Instead, I can tell what I done in the project.

#### "Why I am here and why I need to rewrite AI"

My entire work here was basically rewriting old AI based mostly on ifs to Behavior Tree based one.
The old AI was really badly made, entire behaviour tree was based on ``if()``s, AI base class had around 2.5k lines of code and was scattered in 3 classes. There were also a lot of unneeded polimorphism:
<br>
- "Unit" base class, that was inherited by the AI and Player containing health and some animation logic
<br>
- "AI" base class that inherited from unit base class, containing AI logic and behavior tree made using ``if() else``
<br>
- "Specific AI" (Ranged, melee etc.) class that inherited from AI base class, that implemented some specific changes in AI base class for various types of opponents
<br>
Most of the old code got deleted except a few parts like executing attacks and defence logic and some code around animation controller.

#### New AI

AI was made in modular way. I created low level components for stuff like health and animation playing, then on top I added code that reacted to states defined in behaviour tree.
Becouse the AI in this project mostly fight in groups (group vs group) and there will be no stealth in game I decided to skip entire unit perception mechanic and instead create group controller that will take care of AI perception and attack target choosing.
I also made a modular spawn system for the AI. Entire system was splitted into main component, and 2 modules - trigger and driver. Driver was a component that implemented logic when and what spawns after the trigger call.