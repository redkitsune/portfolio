---
title: Autopsy Simulator
layout: project_post
published: true
post-image: "https://cdn.akamai.steamstatic.com/steam/apps/1283230/header.jpg"
description: Autopsy Simulator is a first-person horror-sim hybrid where players take on the role of a practice pathologist and study case files, made by Woodland Games.
---

**Technologies:**<br>
- Unity
<br>
- C#
<br>
- Behavior Designer 
<br>
- Custom Editor Scripts

---

<div> Due to the fact that I worked here for almost 2 years, the project page is divided into 3 sections: </div>

- <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#Minigames">Minigames</a>
  
- <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#UI">UI</a>
  
- <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#Tools">Tools</a>

Most of the videos here doesn't show proper flow of the game, this is visible mostly in Clipboard video where the entire video is just clipboard being shown over the corpse.
To do this I coded and used <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#Templates">Template System</a> for effortless changing of the game flow. Entire game flow was divided in the parts and we used template system to remove certain parts of the flow to speedup testing and iteration over game content.

(All videos were recorded in unity editor, in my free time, right before release and I forgot to disable debug info in editor)

<section id="Minigames"></section>

# Minigames

### Pipette minigame

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/QkAVsMRQtdc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Sewing & Store body

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/fYVACKr8Pgs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Spine cutting and basement door opening

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/YAXRREt5c1o " title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<section id="Centrifuge"></section>

### Centrifuge and blackout mechanics

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/-1uWBSQ_mtI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Centrifuge might be the more interesting part of this video, but more technicality was put into lights flickering.
In the task I got a hint from our lead designer Michał that light flickering effect from Half Life looks really cool, and we could use something like that.
So after some googling I found <a href="https://www.alanzucconi.com/2021/06/15/valve-flickering-lights/">this</a> that explained really well how light flickering in Source games works.
Armed with this code I ported it to Unity, aaand effect was decent, except when we set update rate of logic to more than 10 times per second, at higher rates it changed to stroboscope.
To mitigate this I added lerping and sequence offset, to smooth light changes and to allow the same pattern on multiple lights but with offset pattern start.

### Telephone animation in complex and autopsy

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/qZFvsLNJSsQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

There is one more animation in office and in apartment, but both of them are just playing animation and audio. This animation was much more complicated than the other ones.

To allow player to answer phone from different angles I used 2 different systems. 
First one was focus point tracking based on Cinemachine which allowed me to seamlessly transition from normal camera to camera that will track whatever object I want.
Second one was using navmesh and agent to move player to the desired position in front of phone that will match up with phone answer animation.
This 2 systems allowed me to get perfect phone answer animation with whatever position and rotation the player was in.

### Camera minigame

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/Wo7zH7vN0XA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Trolley push

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/S20dl4_huXE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<section id="XRay"></section>

### X-Ray minigame

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/2vWwnHbsEyM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Locker code minigame

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/agYMnvL7q8c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

The locker minigame is part of a blood sucking minigame. I also made part of bloodsucking, but during development of it I got sick and other programmer took responsibility to do it.
I made prototype of it, but im not able to show how it looked. (unless I want to go back 1k commits and different version of unity)

### Blackboard

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/Oa7_nuPs9WM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<section id="UI"></section>

# UI

### Clipboard

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/oXwVIzZlsmc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Main menu

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/XHvV4qlh5-k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### PC Teeth & Fingerprint

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/A2WT0Gy2l6c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/bfhx9LYomnc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

Part of UI for PC minigame is also visible during <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#XRay">X-Ray minigame</a>.

### Police folder

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/iFhZSB4ijI4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

The blur behind police folder element inspection is made using sprites due to the fact that making UI only blur is not an easy task in Unity HDRP.

### Microscope

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/hIKuHs_t0bs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

### Notebook and Read mode
This was a simple notebook that you can see during <a href="{{site.url}}{{site.baseurl}}{{page.url}}/#Centrifuge">Centrifuge minigame</a>. Its just a simple UI where the sprites change when pressing Q or E.
Read mode is a special UI that shows localized content of notebook or inspected item like a note.

<section id="Tools"></section>

# Tools

### Animation system

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_animation_system.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_animation_tree_baking.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_animation_invoker.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>
    </div>
</div>

Animation system consisted on 2 parts:

- the animator params reader + animation interaction params which bakes the data from Animator into scriptable objects, and allows creating interaction packages made of that baked data.
  
- player animation invoker, which allows executing animation interaction params, but also sets player lerping, head movement lerping and a lot of other settings.

This allowed our animator to implement most of the animations (and even entire minigames like body bag minigame, where we inspect clothes in chapter 3 autopsy room) without no help from the programmers.

### Scare system

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_stress_elements1.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_stress_elements2.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

    </div>
</div>

Scare system is a system that allows designers to change sanity and stress levels when scare or minigame fail state occurs. High stress levels changes the vision of player and ambient sounds. High sanity levels can lead to being unable to enter some of the minigames or change game ending.
To mitigate this player can take pills to lower stress and sanity.

### MonitoringSuite

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_monitoring_suite1.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_monitoring_suite2.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

    </div>
</div>

This was tool created specifically in my free time to allow us to see what changes in game when we play. It allows to record changes to specific variables and captures stacktraces when that change occurs.
We plugged out flags system, cursor, UI and couple other systems to it.

<section id="Templates"></section>

### Toolbar, Templates and in-game Chapters Menu

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_chapter_templates.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_toolbar.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_chapters_menu.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

    </div>
</div>

Templates were created to alter flow of the game for faster testing of specific minigames.
Toolbar was made to allow easier and faster access to certain subchapters. "S" allows to inspect SO with subchapter data, "Load" loads chapter in editor, "Keep&Run" saves current scenes and runs subchapter, "Run" just runs it.
Chapters menu is runtime only menu that allows to load different subchapters in runtime/playmode. It was created using <a href="https://github.com/nothke/ProtoGUI">ProtoGUI</a> but this wasnt a good idea due to the fact that its pretty limited,
so I created small asset for creating debug windows using DearImgui.

### Flags search & Flags Inspector

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_flags_inspector.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_flags_search1.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_flags_search2.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

    </div>
</div>

Flag inspector is created to allow changing flag value and to see what is the value of a flag.
Flags search is a window to allow searching for flags, Scenes view basically scans entire Assembly for all the classes that uses FlagEntry class and then scans entire Scenes to get the components to display them in window.
Behavior Trees view was easier to implement becouse we only have 4 nodes that utilise flags system so its way faster to scan BTs and read flag entries.

### Behavior Tree Runner improvement

There were couple changes made to Behavior Designer and how we access it.
First I added the "BT Runner" button to focus our Behavior Tree Runner in inspector.
Second I added code to make Behavior Designer window react to Behavior Tree changing, previously the window will turn blank when we changed to different tree.
+ some other smaller improvements to nodes
    
### Other Tools
I also created multiple smaller things, like buttons for compiling various versions of our project or buttons to open/delete settings file.