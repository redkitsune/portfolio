---
title: Stadium Renovator
layout: project_post
published: true
post-image: "https://cdn.cloudflare.steamstatic.com/steam/apps/948880/header.jpg"
description: Game about renovating stadiums, made by Nesalis Games. The first commercial project I was in.
---

**Technologies:**<br>
- Unity
<br>
- C#
<br>
- UniRX 
<br>
- Custom Editor Scripts

---

Here is videos and pictures of stuff that I implemented in the project.

#### Presentation of core mechanics

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/w1320iHAXy4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

#### Improved core mechanics and basic gameloop

<p align="center"><iframe style="max-width: 100%; max-height: 100%;" width="1300" height="750" src="https://www.youtube.com/embed/8NufVaCz_jQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

#### Concept of new UI, complete redesign and rewrite of UI

<p align="center"><img src="{{'/assets/images/sr/w1.jpg' | absolute_url }}" style="max-width: 100%"/></p>