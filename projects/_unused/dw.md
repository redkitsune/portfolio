﻿---
title: Micro Inject
layout: project_post
published: true
post-image: /assets/images/microinject/w1.jpg
description: Simple Dependency Injection tool for Unity, to get rid of singletons and allow projects to be more flexible from code perspective.
---

**Technologies:**<br>
- Unity
  <br>
- C#

**Links:**<br>
<a href="https://github.com/FurrField-Studio/MicroInject/">GitHub repo</a>
<br>
<a href="https://furrfield-studio.github.io/MicroInject-Docs/">Documentation</a>

---

### Why I created this tool
In gamedev most of the world managers relay on Singleton pattern, which causes a lot of ``WorldManager.Singleton/Instance`` calls in code and makes it harder to see relations between components.

This is why I created MicroInject to get rid of those Singleton/Instance calls, instead you can mark classes and variables using attributes which will tell the system what components can be injected and where to inject those components.

Currently the tool works, but has one flaw that makes the tool really hard to use, becouse of the attributes system. The user cant clearly see what  dependencies are available to inject at given moment. (tool has support for registering and injecting new dependencies at runtime) To fix it I come with idea to create dependency tracking system coupled with custom inspector to easily see what dependency can be injected.