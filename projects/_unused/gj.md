---
title: Gamify Job
layout: project_post
published: true
post-image: /assets/images/gj/tn_crop.jpg
description: App which tracks your activity durring task execution and sends the data to the server. Capable of measuring progress and activity for multiple users and synchronizing the data on the server. Made in WPF and ASP.Net.
---

###**Links:**<br>

Backend repository: <a href="https://github.com/RedTheKitsune/-GamifyJobBackend-">GitHub</a> <br>
App repository: <a href="https://github.com/RedTheKitsune/GamifyJobApp">GitHub</a> <br>

###**Technologies:**<br>

**Server:**<br>
- .NET 6, ASP .NET Core<br>
- Entity Framework Core 6 (database first approach)<br>
- Swagger (documenting endpoints + client generation)<br>
- MessagePipe (faster replacement for MediatR)<br>
- ZLogger (logging)<br>
- Fluent Result (generalised Result object for business code)<br>
- LitJWT (session token generator)<br>
- Jdenticon (default avatar generator)<br>
- BCrypt + AES (BCrypt for password hashing, AES for email encryption)<br>
- Consul (health checks + configuration)<br>
- Rin (execution time tracking for endpoints, development environment only)<br>

**Client:**<br>
- WPF + .NET Generic Host<br>
- MessagePipe (faster replacement for MediatR)<br>
- ZLogger (logging)<br>
- Fluent Result (generalised Result object for business code)<br>
- LiveChartsCore (displaying session chart)<br>

**Database:** Postgres

---

### Server

Endpoints using Swagger UI. <br>
<p align="center"><img src="{{'/assets/images/gj/swagger.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<br>

Rin UI for viewing endpoint execution time. Long "Verify" step execution is caused by BCrypt verifying provided password with the one in database. <br>
<p align="center"><img src="{{'/assets/images/gj/rin.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<br>

#### Architecture

Backend is divided into 5 projects:
- Shared, for data validation and shared classes between client and backend. <br>
- Main, containing Bootstrap, Logging, DI and Controllers. <br>
- Application, containing custom errors for Result object and MessagePipe Commands and Handlers. <br>
- Core, containing Database Context, Entities, DTOs, Class Extensions, interfaces for Repositories and Services. <br>
- Infrastructure, containing implementations of Repositories and Services. <br>

<p align="center"><img src="{{'/assets/images/gj/arch.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<br>

#### New features and improvements to add

- Support for journal session name, description and client applications running time. (these are only implemented in client for now)
- Compacting old journal sessions, sessions older than e.g. 1 week should be compacted into day journal sessions etc. (needs Hangfire daily cronjob implementation or better solution)
- Support for client autologin using special JWT token.

### Client

App toolbar is completely custom, WinAPI is used for getting blurred background and proper window maximizing.

Login view <br>
<p align="center"><img src="{{'/assets/images/gj/vlogin.jpg' | absolute_url }}" style="max-width: 100%"/></p>

Main view with default user avatar (Jdenticon) and avatar change toolbar<br>
<p align="center"><img src="{{'/assets/images/gj/vmainavatar.jpg' | absolute_url }}" style="max-width: 100%"/></p>

Time and activity tracking enabled. App uses WinAPI global hooks to get mouse and button clicks outside window.<br>
<p align="center"><img src="{{'/assets/images/gj/vmainw.jpg' | absolute_url }}" style="max-width: 100%"/></p>

Journal view, with journal session expanded. Chart is rendered using LiveChartsCore. Start/End time are corrected to match PC timezone (Backend saves DateTime in UTC format)<br>
<p align="center"><img src="{{'/assets/images/gj/vjournal.jpg' | absolute_url }}" style="max-width: 100%"/></p>

#### New features and improvements to add

- Support for adding journal session name and description.
- Autologin.
- Better Start/Stop session button.
- Options view.
- Localization.
- (Maybe) Tracking "focused on" window time.
- (Maybe) Getting process window title, so app can use it to grab project name.