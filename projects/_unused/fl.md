---
title: Frozen Lands
layout: project_post
published: true
post-image: /assets/images/fl/tn.jpg
description: First person survival-horror shooter. Inspired by "Escape from Tarkov" and "STALKER".
---

**Technologies:**<br>
- Unity
<br>
- C#
<br>
- UniTask & UniRX 
<br>
- Custom Editor Scripts

---

Currently we are working on gameplay aspects of the game so there is not a lot to showoff, but here are some screenshots from tutorial and first location. <br>
There is still a lot to do 
<p align="center"><img src="{{'/assets/images/fl/tn.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<p align="center"><img src="{{'/assets/images/fl/wp5.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<p align="center"><img src="{{'/assets/images/fl/wp1.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<p align="center"><img src="{{'/assets/images/fl/wp2.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<p align="center"><img src="{{'/assets/images/fl/wp3.jpg' | absolute_url }}" style="max-width: 100%"/></p>
<p align="center"><img src="{{'/assets/images/fl/wp4.jpg' | absolute_url }}" style="max-width: 100%"/></p>