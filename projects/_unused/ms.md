﻿---
title: Monitoring Suite
layout: project_post
published: true
post-image: /assets/images/as/as_monitoring_suite1.jpg
description: Monitoring Suite is simple event/changes tracking tool for Unity Engine.
---

**Technologies:**<br>
- Unity
  <br>
- C#

**Links:**<br>
<a href="https://github.com/FurrField-Studio/MonitoringSuite">GitHub repo</a>

---

### Why I created this tool

It was mostly a need in the Autopsy Simulator project to log detailed flow of the game, and what is happening inside (what causes UI to open, flag to change etc.)

<div class="container">
    <div class="columns is-centered">

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_monitoring_suite1.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

        <div  style="margin: 1rem;">
            <p><img src="{{'/assets/images/as/as_monitoring_suite2.jpg' | absolute_url }}" style="max-width: 100%"/></p>
        </div>

    </div>
</div>
